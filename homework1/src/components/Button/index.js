const Button = ({ text = "Button", className, onClick = () => {} }) => {
  return (
    <button
      onClick={() => {
        onClick();
      }}
      className={className}
    >
      {text}
    </button>
  );
};

export default Button;
