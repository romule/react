import React from "react";
import PropTypes from "prop-types";

class Modal extends React.PureComponent {
  render() {
    const { header, closeButton, text, closeModalHandler } = this.props;

    return (
      <div
        className="modal-container"
        onClick={(e) => {
          e.target === e.currentTarget && closeModalHandler();
        }}
      >
        <div className="modal">
          {closeButton && (
            <span
              onClick={() => {
                closeModalHandler();
              }}
              className="modal__closeBtn"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="21.414"
                height="21.414"
                viewBox="0 0 21.414 21.414"
              >
                <g id="cross" transform="translate(-566.293 -67.293)">
                  <g
                    id="cross-2"
                    data-name="cross"
                    transform="translate(461.611 -313.17)"
                  >
                    <path
                      id="_11"
                      data-name="11"
                      d="M125.389,400.37l-9.2-9.2,9.2-9.2-.8-.8-9.2,9.2-9.2-9.2-.8.8,9.2,9.2-9.2,9.2.8.8,9.2-9.2,9.2,9.2Z"
                      fill="#333"
                      stroke="#707070"
                      strokeWidth="1"
                    />
                  </g>
                  <path
                    id="Color_Overlay"
                    data-name="Color Overlay"
                    d="M577,78.8,567.8,88l-.8-.8,9.2-9.2L567,68.8l.8-.8,9.2,9.2,9.2-9.2.8.8L577.8,78l9.2,9.2-.8.8Z"
                    fill="#fff"
                  />
                </g>
              </svg>
            </span>
          )}

          <div className="modal__header">{header}</div>
          <p className="modal__text">{text}</p>

          <div className="modal__btnContainer">
            <button>Ok</button>

            {closeButton && (
              <button
                onClick={() => {
                  closeModalHandler();
                }}
              >
                Cancel
              </button>
            )}
          </div>
        </div>
      </div>
    );
  }
}

Modal.defaultProps = {
  header: "No header",
  closeButton: false,
  text: "No text",
  closeModalHandler: () => {},
};

Modal.propTypes = {
  header: PropTypes.string.isRequired,
};
export default Modal;
