import React from "react";

class HeaderClass extends React.PureComponent {
  render() {
    const { logoText } = this.props;

    return (
      <header className="header">
        <h1>{logoText}</h1>
      </header>
    );
  }
}

export default HeaderClass;
