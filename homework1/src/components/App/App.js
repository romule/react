import HeaderClass from "../HeaderClass";
import Button from "../Button";
import Modal from "../ModalClass";
import React from "react";

class App extends React.PureComponent {
  state = {
    firstModal: false,
    secondModal: false,
    delModal: false,
  };

  firstModalHandler = () => {
    this.setState((state) => {
      return {
        ...state,
        firstModal: !state.firstModal,
      };
    });
  };

  secondModalHandler = () => {
    this.setState((state) => {
      return {
        ...state,
        secondModal: !state.secondModal,
      };
    });
  };

  delModalHandler = () => {
    this.setState((state) => {
      return {
        ...state,
        delModal: !state.delModal,
      };
    });
  };

  render() {
    return (
      <>
        <div className="App">
          <HeaderClass logoText="Please Click on button" />

          <div className="btn_container">
            <Button
              className="btn__green"
              text="Open first modal"
              onClick={this.firstModalHandler}
            />
            <Button
              className="btn__red"
              text="Open second modal"
              onClick={this.secondModalHandler}
            />
          </div>

          {this.state.firstModal && (
            <Modal
              text="Once you delete this file, it won’t be possible to undo this action. 
              Are you sure you want to delete it?"
              header="Do you want to delete this file?"
              closeButton={true}
              closeModalHandler={this.firstModalHandler}
            />
          )}

          {this.state.secondModal && (
            <Modal
              text=" Swear to me! It's ends here. Bruce Wayne, eccentric billionaire. It's not who I am underneath but what I do that defines me. Swear to me! No guns, no killing. Well, you see... I'm buying this hotel and setting some new rules about the pool area."
              header="Second modal"
              closeButton={true}
              closeModalHandler={this.secondModalHandler}
            />
          )}
        </div>
      </>
    );
  }
}

export default App;
