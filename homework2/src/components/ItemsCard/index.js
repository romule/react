import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import Card from "../Card";

class Items extends PureComponent {
  render() {
    const { data } = this.props;
    return data.map((item) => {
      return <Card data={item} key={`${item.id}`} />;
    });
  }
}

Items.propTypes = {
  data: PropTypes.array.isRequired,
};

export default Items;
