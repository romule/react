import HeaderClass from "../HeaderClass";
import Items from "../ItemsCard";
import React from "react";

class App extends React.PureComponent {
  state = {
    items: [],
  };
  // constructor(props) {
  //   super(props);
  //   this.state = { firstModal: false, secondModal: false };
  // }

  componentDidMount() {
    // fetch("https://my-json-server.typicode.com/romule/db/items")
    fetch("db.json")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState((state) => {
          return {
            ...state,
            items: [...data],
          };
        });
      });
  }

  render() {
    return (
      <>
        <div className="App">
          <HeaderClass logoText="Car shop" />

          <div className="cards">
            <ul className="cards__list">
              <Items data={this.state.items} />
            </ul>
          </div>
        </div>
      </>
    );
  }
}

export default App;
