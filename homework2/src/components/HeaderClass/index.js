import React from "react";

class HeaderClass extends React.PureComponent {
  render() {
    // console.log(this);
    const { logoText = "Header not set" } = this.props; //object.properties = object.arguments
    return (
      <header className="header">
        <h1>{logoText}</h1>
      </header>
    );
  }
}

export default HeaderClass;
