import React, { PureComponent } from "react";

class Button extends PureComponent {
  render() {
    const { text, className, onClick } = this.props;

    return (
      <button
        onClick={() => {
          onClick();
        }}
        className={className}
      >
        {text}
      </button>
    );
  }
}

Button.defaultProps = {
  text: "no-Button",
  className: "btn",
  onClick: () => {},
};

export default Button;
