import React, { useEffect, useState } from "react";
import Header from "../Header";
import Items from "../ItemsCard";

function App() {
  const [items, setFetchData] = useState(null);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    fetch("https://my-json-server.typicode.com/romule/db/items") // fetch("db.json")
      .then((res) => {
        setLoading(true);
        return res.json();
      })
      .then((data) => {
        setFetchData(data);
        setLoading(false);
      })
      .catch(console.error);
  }, []);

  if (items)
    return (
      <div className="App">
        <Header />

        <main className="cards-list">
          {isLoading ? <h2>Loading...</h2> : <Items data={items} />}
        </main>
      </div>
    );
  return null;
}

export default App;
